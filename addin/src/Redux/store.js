import {createStore, combineReducers,applyMiddleware} from 'redux';
import logger from 'redux-logger';
import {composeWithDevTools} from 'redux-devtools-extension';
import app from './app/appReducer';

const allReducers = combineReducers({app});
const store = createStore(allReducers, composeWithDevTools(applyMiddleware(logger)));
export default store;