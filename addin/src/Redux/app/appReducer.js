const {AppActionTypes} = require('../ActionTypes');

const initialState = {
    orderNo: '',
    waitingClients: [],
    currentClient: {},
    message: '',
    messages: [],
    socket: () => {}
};

const appReducer = (state = initialState, action) => {
    switch(action.type) {
        case AppActionTypes.SETORDERNO:
            return {
                ...state,
                orderNo: action.payload
            };
        case AppActionTypes.SETWAITINGCLIENTS:
            return {
                ...state,
                waitingClients: action.payload
            };
        case AppActionTypes.SETCURRENTCLIENT:
            return {
                ...state,
                currentClient: action.payload
            };
        case AppActionTypes.SETMESSAGE:
            return {
                ...state,
                message: action.payload
            };
        case AppActionTypes.PUSHMESSAGE:
            return {
                ...state,
                messages: [...state.messages, action.payload]
            };
        case AppActionTypes.SETSOCKET:
            return {
                ...state,
                socket: action.payload
            };
        default: return state;
    }
};

export default appReducer;