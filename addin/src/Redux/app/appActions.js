import {AppActionTypes} from '../ActionTypes';

export const setOrderNo = (orderNo = '') => {
    return {
        type: AppActionTypes.SETORDERNO,
        payload: orderNo
    };
};

export const setWaitingClients = (waitingClients = []) => {
    return {
        type: AppActionTypes.SETWAITINGCLIENTS,
        payload: waitingClients
    };
};

export const setCurrentClient = (currentClient = {}) => {
    return {
        type: AppActionTypes.SETCURRENTCLIENT,
        payload: currentClient
    };
};

export const setMessage = (message = '') => {
    return {
        type: AppActionTypes.SETMESSAGE,
        payload: message
    };
};

export const pushMessage = (message = {}) => {
    return {
        type: AppActionTypes.PUSHMESSAGE,
        payload: message
    };
};

export const setSocket = (socket = () => {}) => {
    return {
        type: AppActionTypes.SETSOCKET,
        payload: socket
    };
};