import React, {useEffect} from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import store from './Redux/store';
import { Provider } from 'react-redux';
import {useSelector, useDispatch} from 'react-redux';
import {setSocket} from './Redux';
import openSocket from 'socket.io-client';

import Navigation from './components/Navigation';
import WaitingClients from './views/WaitingClients';
import Chat from './views/Chat';

const ON_LOAD_CONTENT_EVENT = 'onLoadContent';

window.addEventListener(ON_LOAD_CONTENT_EVENT, (e) => {
  ReactDOM.render(
    <Provider store={store}>
      <App/>
    </Provider>,
    document.getElementById('controlAddIn')
  );
});

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    const socket = openSocket('https://bcchat.devhomelab.co.uk', {
      transports: ['websocket']
    });

    dispatch(setSocket(socket));
  }, []);

  return (
    <div>
      <Router>
        <Navigation />
        <Switch>
          <Route path="/" exact><WaitingClients /></Route>
          <Route path="/startChat" exact><Chat /></Route>

          {/* <OnlineRoute path="/login" exact render={(props) => <Login />} /> */}
          {/* <ProtectedRoute path="/startChat" exact render={(props) => <Chat socket={this.socket}/>} /> */}
        </Switch>
      </Router>
    </div>
  );
}

export default App;
