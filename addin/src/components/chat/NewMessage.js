
/* eslint-disable no-unused-vars */
import React from 'react';

import {useSelector, useDispatch} from 'react-redux';
import {pushMessage, setMessage} from '../../Redux';

import { Form, FormControl, InputGroup, Button, Table } from 'react-bootstrap';
import './styles/ChatHistory.css';

class FormatMessage {
    constructor(message, own = true) {
        this.text = message;
        this.own = own;
    }
}

function NewMessage() {
    const newMessage = (e) => {
        e.preventDefault();
        const newMessage = new FormatMessage(states.message);
        console.log(newMessage);
        dispatch(pushMessage(newMessage));
        states.socket.emit('SENDMESSAGE_BC', newMessage);
        dispatch(setMessage(''));
        // props.socket.emit('SENDMESSAGE', states.message);
    };

    const states = useSelector(state => {
        return {
            message: state.app.message,
            messages: state.app.messages,
            socket: state.app.socket
        };
    });

    const dispatch = useDispatch();

    return (
        <div className="new-message">
            <Form onSubmit={newMessage}>
                <InputGroup>
                    <FormControl
                        type="text"
                        name="newMessage"
                        value={states.message}
                        placeholder="Message"
                        className="mr-sm-2"
                        onChange={(e) => dispatch(setMessage(e.target.value))}
                    />
                    <InputGroup.Append>
                        <Button
                            type="submit"
                            variant="outline-success"
                        >Send!</Button>
                    </InputGroup.Append>
                </InputGroup>
            </Form>
        </div>
    );
}

export default NewMessage;