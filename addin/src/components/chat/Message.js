
/* eslint-disable no-unused-vars */
import React from 'react';

import './styles/ChatHistory.css';

function Message(props) {
    return (
        <div className="chat-container">
            <div className={props.own ? 'message-s' : 'message-r'}>
                {props.message}
            </div>
        </div>
    );
}

export default Message;