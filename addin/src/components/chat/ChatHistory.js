
/* eslint-disable no-unused-vars */
import React from 'react';

import {useSelector} from 'react-redux';

import './styles/ChatHistory.css';

import Message from './Message';

function ChatHistory(props) {
    const states = useSelector(state => {
        return {
            message: state.app.message,
            messages: state.app.messages
        };
    });

    return (
        <div className="chat-window" id="chatHistory">
            {states.messages.map((message, i) => {
                console.log(message);
                return <Message key={i} message={message.text} own={message.own} />;
            })}
        </div>
    );
}

export default ChatHistory;