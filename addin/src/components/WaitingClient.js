import React from 'react';
import {Card, Button} from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import {useSelector, useDispatch} from 'react-redux';
import { setCurrentClient } from '../Redux';

class CurrentClient {
    constructor(client) {
        this.clientContactNo = client.clientNo;
        this.clientName = client.clientName;
        this.waitingSince = client.waitingSince
    }
}

function WaitingClient(props) {
    const history = useHistory();
    const dispatch = useDispatch();

    const states = useSelector(state => {
        return {
            socket: state.app.socket
        };
    });

    const handleClick = () => {
        const currentClient = new CurrentClient(props);
        dispatch(setCurrentClient(currentClient));
        states.socket.emit('CONNECTCLIENT_BC', currentClient);
        history.push('/startChat');
    }

    return (
        <div className="contianer">
            <Card>
                <Card.Header>{props.clientNo}</Card.Header>
                <Card.Body>
                    <Card.Title>{props.clientName}</Card.Title>
                    <Card.Text>Waiting since {props.waitingSince.toLocaleString('en-GB', { timeZone: 'UTC' })}</Card.Text>
                    <Button variant="primary" style={{width: '100%'}} onClick={() => handleClick()}>Connect to client</Button>
                </Card.Body>
            </Card>
        </div>
    );
}

export default WaitingClient;
