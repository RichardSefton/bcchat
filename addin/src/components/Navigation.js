/* eslint-disable no-unused-vars */
import React, { Component } from 'react';

import { connect } from 'react-redux';
// import { isAuthenticated, updateBasketItems } from '../Redux';

import { Navbar, Nav } from 'react-bootstrap';

import { BrowserRouter as Router, Link } from 'react-router-dom';

// import './navigation.css';

const mapStateToProps = (state) => {
    return {
      
    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

class Navigation extends Component {
    handleLogout() {

    }

    render() {
        return (
            <Navbar bg="light" expand="lg">
                <Navbar.Brand><Link to="/" style={{ color: '#000035' }}>BC Chat Client</Link></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" style={{ color: 'black' }} />
                <Navbar.Collapse id="basic-navbar-nav" >
                    <Nav className="mr-auto">
                        <Link to="/" style={{margin: '10px'}}>Clients Waiting</Link>
                        <Link to="/startChat" style={{margin: '10px'}}>Chat</Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Navigation);