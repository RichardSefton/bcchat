/* eslint-disable no-unused-vars */
import React, { Component } from 'react';

import { connect } from 'react-redux';
import { setWaitingClients } from '../Redux';
import './styles/WaitingClients.css';
import { Button } from 'react-bootstrap';

import WaitingClient from '../components/WaitingClient';

const mapStateToProps = (state) => {
    return {
        waitingClients: state.app.waitingClients
    };
};

const mapDispatchToProps = ((dispatch) => {
    return {
        setWaitingClients: (waitingClients = []) => dispatch(setWaitingClients(waitingClients)),
    };
});


class WaitingClients extends Component {
    componentDidMount() {
        const ON_SEND_WAITING_CLIENTS = 'onSendWaitingClients';
        window.addEventListener(ON_SEND_WAITING_CLIENTS, (e) => {
            this.props.setWaitingClients(e.detail);
        });
        window.getWaitingClients();
    }
    
    refreshClientList() {
        window.getWaitingClients();
    }

    render() {
        return (
            <div className="container">
                <h4 className="central-header">Clients Waiting for chat <Button onClick={this.refreshClientList.bind(this)}>Refresh</Button></h4>
                {this.props.waitingClients.map(waitingClient =>
                    <div className="container">
                        <WaitingClient 
                            clientName={waitingClient.clientUserName}
                            clientNo={waitingClient.clientContactNo}
                            waitingSince={new Date(waitingClient.clientWaitingSince)}
                        />
                        <br/>
                    </div>
                )}
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WaitingClients);