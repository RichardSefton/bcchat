/* eslint-disable no-unused-vars */
import React, { Component } from 'react';

import { connect } from 'react-redux';

import ChatHistory from '../components/chat/ChatHistory';
import NewMessage from '../components/chat/NewMessage';
import { pushMessage } from '../Redux';

const mapStateToProps = (state) => {
    return {
        currentClient: state.app.currentClient,
        message: state.app.message,
        socket: state.app.socket
    };
};

const mapDispatchToProps = ((dispatch) => {
    return {
        pushMessage: (message = {}) => dispatch(pushMessage(message))
    };
});

class Chat extends Component {
    componentDidMount() {
        const element = document.getElementById('chatHistory');
        element.scrollTo(0, element.scrollHeight);

        this.props.socket.on('RECEIVEMESSAGE_BC', message => {
            this.props.pushMessage(message);
        });
    }

    componentDidUpdate() {
        const element = document.getElementById('chatHistory');
        element.scrollTo(0, element.scrollHeight);
    }

    render() {
        return (
            <div className="container">
                <ChatHistory/>
                <br/>
                <NewMessage/>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat);