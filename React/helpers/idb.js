export default class IDB {
    constructor(dbName, dbVersion) {
        this.dbName = dbName;
        this.dbVersion = dbVersion;
    }

    /**
     * Creates a key fore
     * @param {String} objectStores 
     */
    createKeyStore(objectStores) {
        const self = window.self;
        if (self.indexedDB) {
            const request = self.indexedDB.open(this.dbName, this.dbVersion);
            request.onupgradeneeded = function (event) {
                const db = event.target.result;
                objectStores.forEach(objectStore => {
                    db.createObjectStore(objectStore, {
                        keyPath: 'id',
                        autoIncrement: true
                    });
                });
            };
        }
    }

    dbInsert(dbTable, payload) {
        const self = window.self;
        if (self.indexedDB) {
            const request = self.indexedDB.open(this.dbName, this.dbVersion);

            request.onsuccess = (event) => {
                const db = event.target.result;
                const transaction = db.transaction(dbTable, 'readwrite');
                const productsStore = transaction.objectStore(dbTable);
                payload.forEach(function (product) {
                    productsStore.add(product); // IDBRequest
                });
            };
        }
    }

    dbCheck(dbTable) {
        return new Promise((resolve, reject) => {
            if (window.indexedDB) {
                const request = window.indexedDB.open(this.dbName, this.dbVersion);
                request.onsuccess = (event) => {
                    const db = event.target.result;
                    try {
                        const transaction = db.transaction(dbTable, 'readwrite');
                        const productsStore = transaction.objectStore(dbTable);
                        const results = productsStore.getAll(); // IDBRequest
                        results.onsuccess = () => {
                            resolve(true);
                        };
                    } catch (err) {
                        reject(err);
                    }
                };
            } else {
                reject(false);
            }
        });
    }


    loadFromDb(dbTable) {
        return new Promise((resolve, reject) => {
            if (window.indexedDB) {
                const request = window.indexedDB.open(this.dbName, this.dbVersion);
                request.onsuccess = (event) => {
                    const db = event.target.result;
                    const transaction = db.transaction(dbTable, 'readwrite');
                    const productsStore = transaction.objectStore(dbTable);
                    const results = productsStore.getAll();
                    results.onsuccess = (event) => {
                        resolve(event.target.result);
                    };
                };
            } else {
                reject('no db available');
            }
        });
    }

    getRecordFromId(dbTable, id) {
        return new Promise((resolve, reject) => {
            if (window.indexedDB) {
                const request = window.indexedDB.open(this.dbName, this.dbVersion);

                request.onsuccess = (event) => {
                    const db = event.target.result;
                    const transaction = db.transaction(dbTable, 'readwrite');
                    const productsStore = transaction.objectStore(dbTable);
                    const results = productsStore.get(id);
                    results.onsuccess = (event) => {
                        resolve(event.target.result);
                    };
                };
            } else {
                reject('no db available');
            }
        });
    }

    dbEdit(dbTable, payload, id) {
        if (window.indexedDB) {
            const request = window.indexedDB.open(this.dbName, this.dbVersion);
            request.onsuccess = (event) => {
                payload.id = Number(id);
                const db = event.target.result;
                const transaction = db.transaction(dbTable, 'readwrite');
                const productsStore = transaction.objectStore(dbTable);
                productsStore.put(payload);
            };
        }
    }

    removeRecord(dbTable, id) {
        if (window.indexedDB) {
            const request = window.indexedDB.open(this.dbName, this.dbVersion);
            request.onsuccess = (event) => {
                const db = event.target.result;
                const transaction = db.transaction(dbTable, 'readwrite');
                const productsStore = transaction.objectStore(dbTable);
                productsStore.delete(id); // IDBRequest
            };
        }
    }
}