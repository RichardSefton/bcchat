/* eslint-disable no-unused-vars */
import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
// import { connect } from 'react-redux';
// import { Button } from 'react-bootstrap';

// import openSocket from 'socket.io-client';

// import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// import IDB from './helpers/idb';
// import ProtectedRoute from './components/ProtectedRoute';
// import OnlineRoute from './components/OnlineRoute';
// import StateManager from './classes/StateManager';
// import Process from './classes/Process';
// import { useSelector, useDispatch } from 'react-redux';
// import { isOnline, buttonClick } from '../Redux';
// import store from '../Redux/store';
// import { Provider } from 'react-redux';

// import Navigation from './components/Navigation';
// import Home from './views/Home';
// import Login from './views/Login';
// import MyBasket from './views/MyBasket';

const ON_LOAD_CONTENT_EVENT = 'onLoadContent';

window.addEventListener(ON_LOAD_CONTENT_EVENT, (e) => {
    ReactDOM.render(
        <React.StrictMode>
            <App/>
        </React.StrictMode>,
        document.getElementById('controlAddIn')

    );
    // ReactDOM.render(
    //     <Provider store={store}>
    //         <React.StrictMode>
    //             <App content={e.detail} />
    //         </React.StrictMode>
    //     </Provider>,
    //     document.getElementById('controlAddIn')
    // );
});

// const mapStateToProps = (state) => {
//     return {
//         online: state.app.online,
//         basketItems: state.basket.basketItems
//     };
// };

// const mapDispatchToProps = (dispatch) => {
//     return {
//         isOnline: (online = false) => dispatch(isOnline(online)),
//         isAuthenticated: (authenticated = false) => dispatch(isAuthenticated(authenticated)),
//         updateBasketItems: (basketItems = []) => dispatch(updateBasketItems(basketItems)),
//         updatePaymentStatus: (status = '') => dispatch(updatePaymentStatus(status))
//     };
// };


//PLAIN. NOTHING BUT A DIV
function App() {
    return(
        <div>
            Hello World!!!
        </div>
    )
}

export default App;


//REDUX STATE
// function App(props) {
//     const dispatch = useDispatch();
//     const states = useSelector(state => {
//         return {
//             online: state.app.online,
//             click: state.app.click
//         };
//     });

//     const manageOnlineState = () => {
//         dispatch(isOnline(navigator.onLine));
//     };

//     useEffect(() => {
//         manageOnlineState();
//     }, []);

//     const updateButtonClick = () => {
//         console.log('clicked');
//         dispatch(buttonClick(states.click + 1));
//     };

//     return (
//         <div>
//             <p>Hello World</p>
//             <Button onClick={updateButtonClick}>Click Me!</Button>
//             <p>Clicked {states.click} times</p>
//         </div>
//     );
// }

// export default App;







// class App extends Component {
//     constructor(props) {
//         super(props);

//         // this.socket = openSocket('https://webprojects-dev.co.uk', {
//         //     transports: ['websocket']
//         // });

//         // this.idb = new IDB('nHancedWebAppsReact', 1);
//         // this.stateManager = new StateManager();
//         // this.process = new Process();

//     }

//     async manageOnlineState() {
//         // this.props.isOnline(navigator.onLine);
//     //     navigator.onLine ? this.socket.connect() : this.socket.disconnect();
//     //     //checkAuthentication
//     //     await this.stateManager.getLoggedInState().then((userState) => {
//     //         this.idb.loadFromDb('clientUser').then((clientUser) => {
//     //             if (clientUser.length > 0) {
//     //                 if (userState.auth) {
//     //                     if (userState.clientUser.userId === clientUser[0].userId) {
//     //                         this.props.isAuthenticated(userState.auth);
//     //                     } else {
//     //                         this.props.isAuthenticated(false);
//     //                         clientUser.forEach(user => {
//     //                             this.idb.removeRecord('clientUser', user.id);
//     //                         });
//     //                     }
//     //                 } else {
//     //                     this.props.isAuthenticated(false);
//     //                 }
//     //             } else {
//     //                 this.props.isAuthenticated(false);
//     //             }
//     //         }).catch((err) => {
//     //             console.log(err);
//     //             this.props.isAuthenticated(false);
//     //         });
//     //     });

//     //     //set basket
//     //     await this.idb.loadFromDb('basketSample').then((clientBasket) => {
//     //         this.props.updateBasketItems(clientBasket);
//     //     });

//     //     //update basket
//     //     if (navigator.onLine) {
//     //         this.process.processBasketLines(this.props.basketItems).then((serverBasket) => {
//     //             serverBasket.forEach((item) => {
//     //                 item.salesHeaderId === '' ?
//     //                     this.idb.dbEdit('basketSample', item, item.id)
//     //                     : this.idb.removeRecord('basketSample', item.id);
//     //             });

//     //             this.idb.loadFromDb('basketSample').then((basketItems) => {
//     //                 this.props.updateBasketItems(basketItems);
//     //             });
//     //         });
//     //     }
//     }

//     runOnMount() {
//         // this.props.isOnline(navigator.onLine);
//         // this.idb.loadFromDb('clientUser').then((clientUser) => {
//         //     clientUser = clientUser[0] ?? {};
//         //     this.socket.on(`PAYSTATUS_${clientUser.userId}`, (socketData) => {
//         //         this.idb.loadFromDb('basketSample').then((dbBasketItems) => {
//         //             this.process.processBasketLines(dbBasketItems).then((serverBasket) => {
//         //                 serverBasket.forEach((line) => {
//         //                     if (line.salesHeaderId) {
//         //                         this.idb.removeRecord('basketSample', line.id);
//         //                     } else {
//         //                         this.idb.dbEdit('basketSample', line, line.id);
//         //                     }
//         //                 });
//         //                 this.idb.loadFromDb('basketSample').then((finalBasket) => {
//         //                     this.props.updateBasketItems(finalBasket);
//         //                     this.props.updatePaymentStatus(socketData.status);
//         //                 }).catch((err) => {
//         //                     console.log(err);
//         //                 });
//         //             }).catch((err) => {
//         //                 console.log(err);
//         //             });
//         //         }).catch((err) => {
//         //             console.log(err);
//         //         });
//         //     });
//         // });
//         // this.manageOnlineState();
//     }

//     componentDidMount() {
//         window.addEventListener('online', this.manageOnlineState.bind(this));
//         window.addEventListener('offline', this.manageOnlineState.bind(this));
//         if (navigator.onLine) { this.runOnMount(); }
//     }

//     render() {
//         return (
//             <div>
//                 {/* <Router>
//                     <Navigation />
//                     <Switch>
//                         <Route path="/" exact><Home /></Route>
//                         <OnlineRoute path="/login" exact render={(props) => <Login />} />
//                         <ProtectedRoute path="/myBasket" exact render={(props) => <MyBasket />} />
//                     </Switch>
//                 </Router> */}
//                 Hello World!
//             </div>
//         );
//     }
// }

// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(App);