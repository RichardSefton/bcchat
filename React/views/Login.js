/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import { Redirect } from 'react-router';

import { connect } from 'react-redux';
import { isAuthenticated } from '../../Redux';

import { Form, FormControl, InputGroup, Button, Alert } from 'react-bootstrap';

import Auth from '../classes/Auth';
import IDB from '../helpers/idb';
import StateManager from '../classes/StateManager';

const mapStateToProps = ((state) => {
    return {
        authenticated: state.user.authenticated
    };
});

const mapDispatchToProps = ((dispatch) => {
    return {
        isAuthenticated: (authenticated) => dispatch(isAuthenticated(authenticated)),
    };
});

class Login extends Component {
    constructor(props) {
        super(props);

        this.idb = new IDB('nHancedWebAppsReact', 1);
        this.stateManager = new StateManager();

        this.state = {
            emailAddress: '',
            password: '',
            authProblem: false,
            error: ''
        };
    }

    handleSubmit(event) {
        event.preventDefault();
        const auth = new Auth(this.state.emailAddress, this.state.password, '/expressOfflinePOC/reactAuthSuccess');
        try {
            auth.validate().login().then((result) => {
                if (result.auth) {
                    result.clientUser.id = 1;
                    this.idb.removeRecord('clientUser', 1);
                    this.idb.dbInsert('clientUser', [result.clientUser]);
                    this.props.isAuthenticated(result.auth);
                } else {
                    this.props.isAuthenticated(result.auth);
                    this.setState({ authProblem: true, error: '[unknown error]' });
                }
            }).catch((err) => {
                this.props.isAuthenticated(false);
                this.setState({ authProblem: true, error: err.toString() });
            });
        } catch (err) {
            this.props.isAuthenticated(false);
            this.setState({ authProblem: true, error: err.toString() });
        }
    }

    componentDidMount() {
        //check the auth status
        this.stateManager.getLoggedInState().then((userState) => {
            this.idb.loadFromDb('clientUser').then((clientUser) => {
                if (clientUser.length > 0) {
                    if (userState.auth) {
                        if (userState.clientUser.userId === clientUser[0].userId) {
                            this.props.isAuthenticated(userState.auth);
                        } else {
                            this.props.isAuthenticated(false);
                            clientUser.forEach(user => {
                                this.idb.removeRecord('clientUser', user.id);
                            });
                            this.idb.dbInsert('clientUser', [{ id: 1 }]);
                        }
                    } else {
                        this.props.isAuthenticated(false);
                    }
                } else {
                    this.props.isAuthenticated(false);
                }
            }).catch((err) => {
                console.log(err);
                this.props.isAuthenticated(false);
            });
        });

    }

    render() {
        return !this.props.authenticated ? (
            <div className="container">
                <Form onSubmit={this.handleSubmit.bind(this)}>
                    <InputGroup className="mb-3">
                        <FormControl
                            type="text"
                            name="emailAddress"
                            placeholder="Email Address"
                            className="mr-sm-2"
                            value={this.state.emailAddress}
                            onChange={(e) => this.setState({ emailAddress: e.target.value })} />
                    </InputGroup>
                    <InputGroup className="mb-3">
                        <FormControl
                            type="password"
                            name="password"
                            placeholder="Password"
                            className="mr-sm-2"
                            value={this.state.password}
                            onChange={(e) => this.setState({ password: e.target.value })} />
                    </InputGroup>
                    <InputGroup>
                        <Button
                            type="submit"
                            variant="success"
                            style={{ width: '100%' }}
                        >Log in</Button>
                    </InputGroup>
                </Form>
                {
                    this.state.authProblem ?
                        <div>
                            <Alert variant="warning">
                                There was a problem logging you in {this.state.error}
                            </Alert>
                        </div>
                        :
                        <div></div>
                }
            </div>
        ) : (
            <Redirect to="/" />
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);