/* eslint-disable no-unused-vars */
import React, { Component } from 'react';

import { connect } from 'react-redux';
import { toggleDiscountList, toggleProductList, updateProductList, updateDiscountList, updateBasketItems } from '../../Redux';

import { Button, Collapse } from 'react-bootstrap';

import IDB from '../helpers/idb';
import Data from '../classes/Data';

import DiscountList from '../components/MyBasket/DiscountList';
import ProductList from '../components/MyBasket/ProductList';
import ItemSearch from '../components/MyBasket/ItemSearch';
import Payment from '../components/MyBasket/Payment';
import Process from '../classes/Process';

const mapStateToProps = (state) => {
    return {
        online: state.app.online,
        productList: state.basket.productList,
        discountList: state.basket.discountList,
        productListToggle: state.basket.productListToggle,
        discountListToggle: state.basket.discountListToggle,
        basketItems: state.basket.basketItems
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateProductList: (productList = []) => dispatch(updateProductList(productList)),
        toggleProductList: (toggle = false) => dispatch(toggleProductList(toggle)),
        updateDiscountList: (discountList = []) => dispatch(updateDiscountList(discountList)),
        toggleDiscountList: (toggle = false) => dispatch(toggleDiscountList(toggle)),
        updateBasketItems: (basketItems = []) => dispatch(updateBasketItems(basketItems))
    };
};

class MyBasket extends Component {
    constructor(props) {
        super(props);

        this.idb = new IDB('nHancedWebAppsReact', 1);
        this.data = new Data();
        this.process = new Process();
    }

    handleProductListClick() {
        if (this.props.online) {
            this.data.getProductList().then((serverProducts) => {
                this.idb.loadFromDb('productList').then((clientList) => {
                    clientList.forEach((item) => {
                        this.idb.removeRecord('productList', item.id);
                    });
                    serverProducts.forEach((item, i) => {
                        item.id = i + 1;
                    });
                    this.idb.dbInsert('productList', serverProducts);
                    this.props.updateProductList(serverProducts);
                    this.props.toggleProductList(!this.props.productListToggle);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        } else {
            this.idb.loadFromDb('productList').then((clientProducts) => {
                this.props.updateProductList(clientProducts);
                this.props.toggleProductList(!this.props.productListToggle);
            }).catch((err) => {
                console.log(err);
                this.props.toggleProductList(!this.props.productListToggle);
            });
        }
    }

    handleDiscountListClick() {
        if (this.props.online) {
            this.data.getDiscountList().then((serverDiscounts) => {
                this.props.updateDiscountList(serverDiscounts.productList);
                this.props.toggleDiscountList(!this.props.discountListToggle);
            }).catch((err) => {
                console.log(err);
                this.props.toggleDiscountList(!this.props.discountListToggle);
            });
        } else {
            this.props.toggleDiscountList(!this.props.discountListToggle);
        }
    }

    render() {
        return (
            <div className="container">
                <Button
                    style={{ width: '100%' }}
                    variant="primary"
                    aria-controls="productListCollapseComponent"
                    aria-expanded={this.props.productListToggle}
                    onClick={this.handleProductListClick.bind(this)}
                >Product List</Button>
                <Collapse in={this.props.productListToggle}>
                    <div id="productListCollapseComponent">
                        <ProductList />
                    </div>
                </Collapse>
                <br /><br />
                <Button
                    style={{ width: '100%' }}
                    variant="primary"
                    aria-controls="discountListCollapseComponent"
                    aria-expanded={this.props.discountListToggle}
                    onClick={this.handleDiscountListClick.bind(this)}
                >Discount List</Button>
                <Collapse in={this.props.discountListToggle}>
                    <div id="discountListCollapseComponent">
                        <DiscountList />
                    </div>
                </Collapse>
                <br /><br />
                <ItemSearch />
                <Payment />
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MyBasket);