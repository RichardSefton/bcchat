/* eslint-disable no-unused-vars */
import React, { Component } from 'react';

import { connect } from 'react-redux';

import { Button } from 'react-bootstrap';
import './Home.css';

const mapStateToProps = (state) => {
    return {
        online: state.app.online
    };
};

class Home extends Component {
    checkSession() {
        fetch('/expressOfflinePOC/reactAuthSuccess', {
            credentials: 'same-origin'
        }).then((response) => {
            return response.json();
        }).then((sessionUsername) => {
            console.log(sessionUsername);
        }).catch((err) => {
            console.log(err);
        });
    }

    render() {
        return (
            <div className="container">
                <h1>Hello! From React</h1>
                <h2>A small change</h2>
                <h3>Online state: {this.props.online.toString()}</h3>
                <Button
                    onClick={this.checkSession}
                    disabled={!this.props.online}
                    className="buttonChanges"
                >Check the session user</Button>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    null
)(Home);