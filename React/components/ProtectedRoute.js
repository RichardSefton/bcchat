/* eslint-disable no-unused-vars */
import React from 'react';

import {Redirect} from 'react-router-dom';

import {useSelector} from 'react-redux';

function ProtectedRoute(props) {
    const authenticated = useSelector(state => state.user.authenticated);
    const Component = props.render;
    return authenticated ? (
        <Component props={props}/>
    ) : (
        <Redirect to={{pathname: '/login'}}/>
    );
}

export default ProtectedRoute;