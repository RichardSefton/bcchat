/* eslint-disable no-unused-vars */
import React from 'react';

import {Redirect} from 'react-router-dom';

import {useSelector} from 'react-redux';

function OnlineRoute(props) {
    const Component = props.render;
    const online = useSelector(state => state.app.online);
    return online ? (
        <Component props={props}/>
    ) : (
        <Redirect to={{pathname: '/'}}/>
    );
}

export default OnlineRoute;