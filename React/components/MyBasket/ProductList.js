/* eslint-disable no-unused-vars */
import React from 'react';
import { useSelector } from 'react-redux';

import { Table } from 'react-bootstrap';

function ItemList() {
    const states = useSelector(state => {
        return {
            productList: state.basket.productList
        };
    });

    return (
        <div className="container">
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Product Id</th>
                        <th>Name</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        states.productList.map((product) => (
                            <tr key={product.productId}>
                                <td>{product.productId}</td>
                                <td>{product.productName}</td>
                                <td>£{(product.unitPrice / 100).toFixed(2)}</td>
                            </tr>
                        ))
                    }
                </tbody>
            </Table>
        </div>
    );
}

export default ItemList;