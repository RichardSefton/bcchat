/* eslint-disable no-unused-vars */
import React from 'react';
import { useSelector } from 'react-redux';

import { Table } from 'react-bootstrap';

function DiscountList() {
    const states = useSelector(state => {
        return {
            online: state.app.online,
            discountList: state.basket.discountList
        };
    });

    return (
        <div className='container'>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Product Id</th>
                        <th>Name</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        states.online ?
                            states.discountList.map((discount) => (
                                <tr key={discount.productId}>
                                    <td>{discount.productId}</td>
                                    <td>{discount.productName}</td>
                                    <td>£{((discount.unitPrice * (1 - (discount.discountPercent / 100))) / 100).toFixed(2)}</td>
                                </tr>
                            )) :
                            <tr>
                                <td colSpan="3">Unable to view Discounts while offline</td>
                            </tr>
                    }
                </tbody>
            </Table>
        </div>
    );
}

export default DiscountList;