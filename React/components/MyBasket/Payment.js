/* eslint-disable no-unused-vars */
import React from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { showPaymentModal, updatePaymentStatus } from '../../../Redux';

import { Button, Modal } from 'react-bootstrap';

import IDB from '../../helpers/idb';
import Process from '../../classes/Process';

function Payment() {
    const idb = new IDB('nHancedWebAppsReact', 1);
    const process = new Process();

    const states = useSelector(state => {
        return {
            online: state.app.online,
            paymentModal: state.basket.paymentModal,
            paymentStatus: state.basket.paymentStatus
        };
    });
    const dispatch = useDispatch();

    const handleClickFailed = () => {
        dispatch(updatePaymentStatus('Processing'));
        dispatch(showPaymentModal(true));
        idb.loadFromDb('basketSample').then((dbBasketItems) => {
            process.processPayment(dbBasketItems, 'failed', idb);
        }).catch((err) => {
            console.log(err);
            dispatch(updatePaymentStatus('Connection Error'));
        });
    };

    const handleClickSuccess = () => {
        dispatch(updatePaymentStatus('Processing'));
        dispatch(showPaymentModal(true));
        idb.loadFromDb('basketSample').then((dbBasketItems) => {
            process.processPayment(dbBasketItems, 'succeeded', idb);
        }).catch((err) => {
            console.log(err);
            dispatch(updatePaymentStatus('Connection Error'));
        });
    };

    return (
        <div className="container">
            <Modal show={states.paymentModal} centered>
                <Modal.Header>
                    Processing Payment
                </Modal.Header>
                <Modal.Body>
                    <div className="container">
                        <b>Please wait...</b>
                        <p>Status: {states.paymentStatus}</p>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        variant="primary"
                        style={{ width: '100%' }}
                        onClick={() => dispatch(showPaymentModal(false))}
                    >Close</Button>
                </Modal.Footer>
            </Modal>
            <Button
                disabled={!states.online}
                variant="success"
                onClick={handleClickFailed}
                style={{ width: '40%', margin: '10px' }}
            >Pay now (Fail)</Button>
            <Button
                disabled={!states.online}
                variant="success"
                onClick={handleClickSuccess}
                style={{ width: '40%', margin: '10px' }}
            >Pay now (Pass)</Button>
        </div>
    );
}

export default Payment;