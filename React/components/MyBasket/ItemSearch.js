/* eslint-disable no-unused-vars */
import React, { useState } from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { updateBasketItems } from '../../../Redux';

import { Form, FormControl, InputGroup, Button, Table } from 'react-bootstrap';

// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faTrash } from '@fortawesome/free-solid-svg-icons';

import IDB from '../../helpers/idb';
import Process from '../../classes/Process';

function ItemSearch() {
    const [searchBox, setSearchBox] = useState('');

    const idb = new IDB('nHancedWebAppsReact', 1);
    const process = new Process();

    const states = useSelector(state => {
        return {
            online: state.app.online,
            basketItems: state.basket.basketItems
        };
    });

    const dispatch = useDispatch();

    const insertLine = async () => {
        const searchTerm = searchBox;
        setSearchBox('');

        let clientUser = {};
        await idb.loadFromDb('clientUser').then((clientUserDb) => {
            clientUser = clientUserDb[0] || {};
        });

        if (searchTerm != '') {
            await idb.loadFromDb('productList').then((clientProducts) => {
                const line = clientProducts.filter((prod) => prod.productId == searchTerm);
                line.length === 1 ? idb.dbInsert('basketSample', [{
                    productId: line[0].productId,
                    productName: line[0].productName,
                    insertDate: new Date(),
                    detailsFound: false,
                    serverId: '',
                    salesHeaderId: '',
                    userId: clientUser.userId
                }]) : idb.dbInsert('basketSample', [{
                    productId: searchTerm,
                    productName: 'waiting for details...',
                    insertDate: new Date(),
                    detailsFound: false,
                    serverId: '',
                    salesHeaderId: '',
                    userId: clientUser.userId
                }]);
            });
        }

        await idb.loadFromDb('basketSample').then((basketItems) => {
            if (states.online) {
                process.processBasketLines(basketItems).then((serverBasket) => {
                    serverBasket.forEach((line) => {
                        if (line.salesHeaderId) {
                            idb.removeRecord('basketSample', line.id);
                        } else {
                            idb.dbEdit('basketSample', line, line.id);
                        }
                    });
                    dispatch(updateBasketItems(serverBasket));
                });
            } else {
                dispatch(updateBasketItems(basketItems));
            }
        }).catch((err) => {
            console.log(err);
        });
    };


    const handleSubmit = (event) => {
        //get value from idb
        event.preventDefault();
        insertLine();
    };

    const deleteLine = (id) => {
        idb.removeRecord('basketSample', id);
        idb.loadFromDb('basketSample').then((dbBasket) => {
            dispatch(updateBasketItems(dbBasket));
        });
    };

    return (
        <div className="container">
            <Form onSubmit={handleSubmit}>
                <InputGroup>
                    <FormControl
                        type="text"
                        name="searchTerm"
                        placeholder="Search"
                        className="mr-sm-2"
                        value={searchBox}
                        onChange={(e) => setSearchBox(e.target.value)} />
                    <InputGroup.Append>
                        <Button
                            type="submit"
                            variant="outline-success"
                        >Search Items</Button>
                    </InputGroup.Append>
                </InputGroup>
            </Form>
            <br /><br />
            <Table>
                <thead>
                    <tr>
                        <th>Product Id</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        states.basketItems.map(item => (
                            <tr key={item.id}>
                                {/* <td>{item.productId}</td>
                                <td>{item.productName}</td>
                                <td>£{isNaN(item.unitPrice) ? '0.00' : (item.unitPrice / 100).toFixed(2)}</td>
                                <td style={{ 'textAlign': 'center' }}>
                                    <a style={{ cursor: 'pointer' }}
                                        onClick={() => deleteLine(item.id)}
                                    ><FontAwesomeIcon
                                            icon={faTrash} />
                                    </a>
                                </td> */}
                            </tr>
                        ))
                    }
                </tbody>
            </Table>
        </div>
    );
}

export default ItemSearch;