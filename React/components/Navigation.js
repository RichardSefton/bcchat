/* eslint-disable no-unused-vars */
import React, { Component } from 'react';

import { connect } from 'react-redux';
import { isAuthenticated, updateBasketItems } from '../../Redux';

import { Navbar, Nav } from 'react-bootstrap';

import Process from '../classes/Process';
import IDB from '../helpers/idb';

import { BrowserRouter as Router, Link } from 'react-router-dom';

import './navigation.css';

const mapStateToProps = (state) => {
    return {
        online: state.app.online,
        authenticated: state.user.authenticated
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        isAuthenticated: (authenticated = false) => dispatch(isAuthenticated(authenticated)),
        updateBasketItems: (basketItems = []) => dispatch(updateBasketItems(basketItems))
    };
};

class Navigation extends Component {
    constructor(props) {
        super(props);

        this.process = new Process();
        this.idb = new IDB('nHancedWebAppsReact', 1);
    }

    handleLogout() {
        if (this.props.online) { this.process.logout(); }
        this.idb.removeRecord('clientUser', 1);
        this.idb.dbInsert('clientUser', [{ id: 1 }]);
        this.idb.loadFromDb('basketSample').then((basketItems) => {
            basketItems.forEach((item) => {
                this.idb.removeRecord('basketSample', item.id);
            });
            this.props.updateBasketItems([]);
            this.props.isAuthenticated(false);
        });
    }

    render() {
        return (
            <Navbar bg="light" expand="lg">
                {/* <Navbar.Brand><Link to="/" style={{ color: '#000035' }}>React-Bootstrap</Link></Navbar.Brand> */}
                {/* <Navbar.Toggle aria-controls="basic-navbar-nav" style={{ color: 'black' }} /> */}
                <Navbar.Collapse id="basic-navbar-nav" >
                    <Nav className="mr-auto">
                        {
                            this.props.authenticated ?
                                <Nav className="mr-auto">
                                    <Nav>
                                        <Link to="/myBasket">My Basket</Link>
                                    </Nav>
                                    <Nav>
                                        <a href="#" onClick={this.handleLogout.bind(this)}>Logout</a>
                                    </Nav>
                                </Nav>
                                :
                                this.props.online ?
                                    <Nav className="mr-auto">
                                        <Nav>
                                            <Link to="/login">Login</Link>
                                        </Nav>
                                    </Nav>
                                    :
                                    <b>You are offline. Please go online to Login...</b>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Navigation);