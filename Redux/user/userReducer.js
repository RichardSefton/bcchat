import { UserActionTypes } from '../ActionTypes';

const initialState = {
    authenticated: false,
};

const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case UserActionTypes.AUTHENTICATED:
            return {
                ...state,
                authenticated: action.payload
            };
        default: return state;
    }
};

export default appReducer;