import { UserActionTypes } from '../ActionTypes';

export const isAuthenticated = (authenticated = false) => {
    return {
        type: UserActionTypes.AUTHENTICATED,
        payload: authenticated
    };
};