import { createStore, combineReducers, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
//reducers
import app from './app/appReducer';
import user from './user/userReducer';
import basket from './basket/basketReducer';

//combine reducers into one
const allReducers = combineReducers({ app, user, basket });

//create the store
const store = createStore(allReducers, composeWithDevTools(applyMiddleware(logger)));

export default store;