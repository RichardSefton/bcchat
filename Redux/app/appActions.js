import { AppActionTypes } from '../ActionTypes';

export const isOnline = (online = false) => {
    return {
        type: AppActionTypes.ONLINE,
        payload: online
    };
};

export const buttonClick = (click = 0) => {
    return {
        type: AppActionTypes.BUTTON,
        payload: click
    };
};