const { AppActionTypes } = require('../ActionTypes');

const initialState = {
    online: false,
    click: 0
};

const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case AppActionTypes.ONLINE:
            return {
                ...state,
                online: action.payload
            };
        case AppActionTypes.BUTTON:
            return {
                ...state,
                click: action.payload
            };
        default: return state;
    }
};

export default appReducer;