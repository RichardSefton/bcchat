const { BasketActionTypes } = require('../ActionTypes');

const initialState = {
    paymentModal: false,
    paymentStatus: '',
    productListToggle: false,
    discountListToggle: false,
    basketItems: [],
    productList: [],
    discountList: []
};

const basketReducer = (state = initialState, action) => {
    switch (action.type) {
        case BasketActionTypes.PAYMENT_MODAL:
            return {
                ...state,
                paymentModal: action.payload
            };
        case BasketActionTypes.PAYMENT_STATUS:
            return {
                ...state,
                paymentStatus: action.payload
            };
        case BasketActionTypes.PRODUCT_LIST_TOGGLE:
            return {
                ...state,
                productListToggle: action.payload
            };
        case BasketActionTypes.DISCOUNT_LIST_TOGGLE:
            return {
                ...state,
                discountListToggle: action.payload
            };
        case BasketActionTypes.BASKET_ITEMS:
            return {
                ...state,
                basketItems: action.payload
            };
        case BasketActionTypes.PRODUCT_LIST:
            return {
                ...state,
                productList: action.payload
            };
        case BasketActionTypes.DISCOUNT_LIST:
            return {
                ...state,
                discountList: action.payload
            };
        default: return state;
    }
};

export default basketReducer;