import { BasketActionTypes } from '../ActionTypes';

export const showPaymentModal = (paymentModal = false) => {
    return {
        type: BasketActionTypes.PAYMENT_MODAL,
        payload: paymentModal
    };
};

export const updatePaymentStatus = (paymentStatus = '') => {
    return {
        type: BasketActionTypes.PAYMENT_STATUS,
        payload: paymentStatus
    };
};

export const toggleProductList = (toggle = false) => {
    return {
        type: BasketActionTypes.PRODUCT_LIST_TOGGLE,
        payload: toggle
    };
};

export const toggleDiscountList = (toggle = false) => {
    return {
        type: BasketActionTypes.DISCOUNT_LIST_TOGGLE,
        payload: toggle
    };
};

export const updateBasketItems = (basketItems = []) => {
    return {
        type: BasketActionTypes.BASKET_ITEMS,
        payload: basketItems
    };
};

export const updateProductList = (productList = []) => {
    return {
        type: BasketActionTypes.PRODUCT_LIST,
        payload: productList
    };
};

export const updateDiscountList = (discountList = []) => {
    return {
        type: BasketActionTypes.DISCOUNT_LIST,
        payload: discountList
    };
};