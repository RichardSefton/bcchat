page 50250 "BC Chat Client User List"
{

    ApplicationArea = All;
    Caption = 'BC Chat Client User List';
    PageType = List;
    SourceTable = "BC Chat Client User";
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Code; Rec.Code)
                {
                    ToolTip = 'Specifies the value of the Code field';
                    ApplicationArea = All;
                    Lookup = true;
                    LookupPageId = "Contact List";
                }
                field("Client User Contact No."; Rec."Client User Contact No.")
                {
                    Lookup = true;
                    LookupPageId = "Contact List";
                    ToolTip = 'Specifies the value of the Client User Contact No. field';
                    ApplicationArea = All;
                }
                field("Client User Name"; Rec."Client User Name")
                {
                    ToolTip = 'Specifies the value of the Client User Name field';
                    ApplicationArea = All;
                }
                field("Client User Username"; Rec."Client User Username")
                {
                    ToolTip = 'Specifies the value of the Client User Username field';
                    ApplicationArea = All;
                }
                field("Client User Last Login"; Rec."Client User Last Login")
                {
                    ToolTip = 'Specifies the value of the Client User Last Login field';
                    ApplicationArea = All;
                }
                field("Client User Last Chat"; Rec."Client User Last Chat")
                {
                    ToolTip = 'Specifies the value of the Client User Last Chat field';
                    ApplicationArea = All;
                }
                field("Client User Last Chat With"; Rec."Client User Last Chat With")
                {
                    ToolTip = 'Specifies the value of the Client User Last Chat With field';
                    ApplicationArea = All;
                }

                field("User Waiting"; Rec."User Waiting")
                {
                    ToolTip = 'Specifies if the user is currently waiting for a chat';
                    ApplicationArea = All;
                }
            }
        }
    }

}
