page 50252 "BC Chat"
{
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;

    layout
    {
        area(Content)
        {
            group("React Control Addin")
            {
                usercontrol("BCC Control Addin"; "BCC Control Addin")
                {
                    ApplicationArea = All;

                    trigger Ready()
                    begin
                        LoadContent();
                    end;

                    trigger GetWaitingClients()
                    var
                        ClientUser: Record "BC Chat Client User";
                        WaitingUsers: JsonArray;
                        BCChatFunctions: Codeunit "BC Chat Functions";
                    begin
                        ClientUser.Reset();
                        ClientUser.SetRange("User Waiting", true);
                        if ClientUser.FindSet() then
                            repeat
                                BCChatFunctions.AddBCCClientUserToJson(ClientUser, WaitingUsers);
                            until ClientUser.Next() = 0;
                        CurrPage."BCC Control Addin".SendWaitingClients(WaitingUsers);
                    end;

                    trigger OpenSalesOrder(OrderNo: Code[20])
                    var
                        SalesOrder: Record "Sales Header";
                        SalesOrderPage: Page "Sales Order";
                    begin
                        if SalesOrder.Get(SalesOrder."Document Type"::Order, OrderNo) then begin
                            SalesOrderPage.SetTableView(SalesOrder);
                            SalesOrderPage.SetRecord(SalesOrder);
                            SalesOrderPage.Run();
                        end;
                    end;
                }
            }
        }
    }

    local procedure LoadContent()
    var
        JObject: JsonObject;
    begin
        JObject.Add('SampleData', 'SomeValue');
        CurrPage."BCC Control Addin".LoadContent(JObject);
    end;
}