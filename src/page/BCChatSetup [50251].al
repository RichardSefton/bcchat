page 50251 "BC Chat Setup"
{
    Caption = 'BC Chat Setup';
    PageType = Card;
    SourceTable = "BC Chat Setup";
    UsageCategory = Administration;
    InsertAllowed = false;
    ApplicationArea = All;

    layout
    {
        area(content)
        {
            group(General)
            {
                field("BC Chat Client User No. Series"; Rec."BC Chat Client User No. Series")
                {
                    ToolTip = 'Specifies the value of the BC Chat Client User No Series';
                    ApplicationArea = All;
                }
            }
        }
    }

    trigger OnInit()
    begin
        if not Rec.Get() then
            Rec.Insert();
    end;

}
