function onLoadContent(content) {
    const event = new CustomEvent('onLoadContent', { detail: content });
    window.dispatchEvent(event);
}

window.addEventListener('onContentChange', function (e) {
    const content = e.detail;
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('OnContentChange', [content])
});

window.loadLogo = () => {
    return Microsoft.Dynamics.NAV.GetImageResource('src/ContentEditor/images/logo.png');
}

window.openSalesOrder = (orderNo) => {
    console.log(orderNo);
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('OpenSalesOrder', [orderNo]);
}

window.getWaitingClients = () => {
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('GetWaitingClients');
}

const onSendWaitingClients = (content) => {
    const event = new CustomEvent('onSendWaitingClients', {detail: content});
    window.dispatchEvent(event);
};

window.SendWaitingClients = (content) => {
    if (JSON.stringify(content) == '{}')
        onSendWaitingClients(null);
    else
        onSendWaitingClients(content);
};

window.LoadContent = function (content) {
    if (JSON.stringify(content) == '{}')
        onLoadContent(null);
    else
        onLoadContent(content);
}