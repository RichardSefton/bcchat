controladdin "BCC Control Addin"
{
    MinimumHeight = 850;
    RequestedHeight = 850;
    MinimumWidth = 700;
    VerticalStretch = true;
    VerticalShrink = false;
    HorizontalStretch = true;
    HorizontalShrink = true;

    Scripts =
        'src/ContentEditor/js/content-editor-events.js',
        'src/ContentEditor/js/content-editor.js';

    StartupScript = 'src/JsScript/Start.js';

    StyleSheets =
        'src/ContentEditor/css/content-editor.css',
        'https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css';

    Images =
        'src/ContentEditor/images/logo.png';

    event Ready();
    event OnContentChange(content: JsonObject);
    event OpenSalesOrder(OrderNo: Code[20]);
    event GetWaitingClients();
    procedure LoadContent(content: JsonObject);
    procedure SendWaitingClients(content: JsonArray);



}