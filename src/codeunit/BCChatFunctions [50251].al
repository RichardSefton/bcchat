codeunit 50251 "BC Chat Functions"
{
    procedure ToggleClientWaiting(data: Text): Text
    var
        ClientUserNo: Code[20];
        Waiting: Boolean;
        BCChatClientUser: Record "BC Chat Client User";
    begin
        if not ReadClientWaitingPayload(data, ClientUserNo, Waiting) then
            exit('No Valid payload');
        if ClientUserNo = '' then
            exit('clientNo cannot be blank');
        BCChatClientUser.Reset();
        if BCChatClientUser.Get(ClientUserNo) then begin
            BCChatClientUser.Validate("User Waiting", Waiting);
            BCChatClientUser.Modify(true);
        end else begin
            exit('Client User not found');
        end;
        exit(StrSubstNo('Waiting status updated to %1', Waiting));
    end;

    [TryFunction]
    local procedure ReadClientWaitingPayload(data: Text; var ClientUserNo: Code[20]; var Waiting: Boolean)
    var
        ClientWaitingPayloadObject: JsonObject;
        ClientNoToken: JsonToken;
        WaitingToken: JsonToken;
    begin
        ClientWaitingPayloadObject.ReadFrom(data);
        if ClientWaitingPayloadObject.Get('clientNo', ClientNoToken) then
            ClientUserNo := ClientNoToken.AsValue().AsCode();
        if ClientWaitingPayloadObject.Get('waiting', WaitingToken) then
            Waiting := WaitingToken.AsValue().AsBoolean();
    end;

    procedure AddBCCClientUserToJson(ClientUser: Record "BC Chat Client User"; var WaitingUsers: JsonArray)
    var
        WaitingUser: JsonObject;
    begin
        WaitingUser.Add('clientUserName', ClientUser."Client User Name");
        WaitingUser.Add('clientContactNo', ClientUser."Client User Contact No.");
        WaitingUser.Add('clientWaitingSince', ClientUser."Client User Last Login");
        WaitingUsers.Add(WaitingUser);
    end;
}