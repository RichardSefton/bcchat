codeunit 50250 "Ugly Authentication"
{
    procedure BCChatCreateClientUser(data: Text): Text
    var
        Name: Text;
        Email: Text;
        PhoneNo: Text;
        Username: Text;
        Password: Text;
    begin
        if not ReadNewClientUser(data, Name, Email, PhoneNo, Username, Password) then
            exit('One of the required keys are not present');
        if Name = '' then
            exit('No name supplied');
        if Email = '' then
            exit('No email supplied');
        if PhoneNo = '' then
            exit('No phone no supplied');
        if Username = '' then
            exit('No username supplied');
        if Password = '' then
            exit('No password supplied');
        exit(ProcessNewUser(Name, Email, PhoneNo, Username, Password))
    end;

    local procedure ProcessNewUser(Name: Text; Email: Text; PhoneNo: Text; Username: Text; Password: Text): Text
    var
        Contact: Record Contact;
        BCChatClientUser: Record "BC Chat Client User";
        NextClientUserCode: Code[20];
        NoSeriesMgt: Codeunit NoSeriesManagement;
        BCChatSetup: Record "BC Chat Setup";
        ContactNo: Code[20];
    begin
        BCChatClientUser.Reset();
        BCChatClientUser.SetRange("Client User Username", Username);
        if BCChatClientUser.FindFirst() then
            exit('User already exists');
        BCChatClientUser.Reset();
        if BCChatSetup.Get() then begin
            NextClientUserCode := NoSeriesMgt.GetNextNo(BCChatSetup."BC Chat Client User No. Series", 0D, true);
            BCChatClientUser.Init();
            BCChatClientUser.Validate(Code, NextClientUserCode);
            BCChatClientUser.Insert(true);
            Clear(NoSeriesMgt);
            BCChatClientUser.Validate("Client User Username", Username);
            BCChatClientUser.Validate("Client User Password", Password);
            Contact.Reset();
            Contact.Init();
            Contact.Validate(Type, Contact.Type::Person);
            Contact.Validate(Name, Name);
            Contact.Validate("Phone No.", PhoneNo);
            Contact.Validate("E-Mail", Email);
            Contact.Insert(true);
            BCChatClientUser.Validate("Client User Contact No.", Contact."No.");
            BCChatClientUser.Modify(true);
        end;
        exit('User Inserted');
    end;

    [TryFunction]
    local procedure ReadNewClientUser(data: Text; var Name: Text; var Email: Text; var PhoneNo: Text; var Username: Text; var Password: Text)
    var
        NewUserObject: JsonObject;
        NameToken: JsonToken;
        EmailToken: JsonToken;
        PhoneNoToken: JsonToken;
        UsernameToken: JsonToken;
        PasswordToken: JsonToken;
    begin
        NewUserObject.ReadFrom(data);
        if NewUserObject.Get('name', NameToken) then
            Name := NameToken.AsValue().AsText();
        if NewUserObject.Get('email', EmailToken) then
            Email := EmailToken.AsValue().AsText();
        if NewUserObject.Get('phoneNo', PhoneNoToken) then
            PhoneNo := PhoneNoToken.AsValue().AsText();
        if NewUserObject.Get('username', UsernameToken) then
            Username := UsernameToken.AsValue().AsText();
        if NewUserObject.Get('password', PasswordToken) then
            Password := PasswordToken.AsValue().AsText();
    end;

    procedure BCChatClientAuthentication(data: Text): Text
    var
        Username: Text;
        Password: Text;
        BCChatClientUser: Record "BC Chat Client User";
    begin
        if not ReadAuthenticationJson(data, Username, Password) then
            exit('One of the required keys are not present');
        if Username = '' then
            exit('Username not supplied');
        if Password = '' then
            exit('Password not supplied');
        exit(Authenticate(CheckClientUserExists(Username, Password, BCChatClientUser), BCChatClientUser))
    end;

    local procedure Authenticate(UserFound: Boolean; BCChatClientUser: Record "BC Chat Client User") Stringified: Text
    var
        AuthObject: JsonObject;
    begin
        AuthObject.Add('auth', UserFound);
        AuthObject.Add('clientUserCode', BCChatClientUser.Code);
        AuthObject.Add('name', BCChatClientUser."Client User Name");
        AuthObject.WriteTo(Stringified);
    end;

    local procedure CheckClientUserExists(Username: Text; Password: Text; var BCChatClientUser: Record "BC Chat Client User"): Boolean
    begin
        BCChatClientUser.Reset();
        BCChatClientUser.SetRange("Client User Username", Username);
        if BCChatClientUser.FindFirst() then
            if BCChatClientUser."Client User Password" = Password then begin
                BCChatClientUser.Validate("Client User Last Login", CurrentDateTime());
                BCChatClientUser.Modify(true);
                exit(true);
            end;
        exit(false);
    end;

    [TryFunction]
    local procedure ReadAuthenticationJson(data: Text; var Username: Text; var Password: Text)
    var
        UsernameToken: JsonToken;
        PasswordToken: JsonToken;
        AuthObject: JsonObject;
    begin
        AuthObject.ReadFrom(data);
        if AuthObject.Get('username', UsernameToken) then
            Username := UsernameToken.AsValue().AsText();
        if AuthObject.Get('password', PasswordToken) then
            Password := PasswordToken.AsValue().AsText();
    end;
}