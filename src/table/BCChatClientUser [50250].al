table 50250 "BC Chat Client User"
{
    Caption = 'BC Chat Client Users';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; Code; Code[20])
        {
            Caption = 'Code';
            DataClassification = ToBeClassified;
        }
        field(10; "Client User Name"; Text[2048])
        {
            Caption = 'Client User Name';
            DataClassification = ToBeClassified;
        }
        field(20; "Client User Contact No."; Code[20])
        {
            Caption = 'Client User Contact No.';
            DataClassification = ToBeClassified;
            trigger OnValidate()
            var
                Contact: Record Contact;
            begin
                if Contact.Get(Rec."Client User Contact No.") then
                    Rec.Validate("Client User Name", Contact.Name);
                Rec.Modify(true);
            end;
        }
        field(30; "Client User Username"; Text[2048])
        {
            Caption = 'Client User Username';
            DataClassification = ToBeClassified;
        }
        field(40; "Client User Password"; Text[2048])
        {
            Caption = 'Client User Password';
            DataClassification = ToBeClassified;
        }
        field(50; "Client User Last Login"; DateTime)
        {
            Caption = 'Client User Last Login';
            DataClassification = ToBeClassified;
        }
        field(60; "Client User Last Chat"; DateTime)
        {
            Caption = 'Client User Last Chat';
            DataClassification = ToBeClassified;
        }
        field(70; "Client User Last Chat With"; Code[20])
        {
            Caption = 'Client User Last Chat With';
            DataClassification = ToBeClassified;
        }
        field(80; "User Waiting"; Boolean)
        {
            Caption = 'Client User Waiting for Chat';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }

        key(Username; "Client User Username")
        {
            Clustered = false;
            Unique = true;
        }
    }
}
