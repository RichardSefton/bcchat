table 50251 "BC Chat Setup"
{
    Caption = 'Service Pleaze Setup';
    DataClassification = ToBeClassified;

    fields
    {
        field(1; ID; Code[10])
        {
            DataClassification = SystemMetadata;
        }
        field(10; "BC Chat Client User No. Series"; Code[20])
        {
            Caption = 'SP No Series';
            DataClassification = ToBeClassified;
        }
    }
    keys
    {
        key(PK; ID)
        {
            Clustered = true;
        }
    }

}